<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <nav class="navbar">
        <a id="book-list-link" href="index.php">Books</a>
        <a id="book-form-link" href="add-book.php">Add book</a>
        <a id="author-list-link" href="authors.php">Authors</a>
        <a id="author-form-link" href="add-author.php">Add author</a>
    </nav>
    <div class="form-div">
        <form action="authors.php">
            <label for="firstName">First name:</label>
            <input type="text" name="firstName" id="firstName" placeholder="Enter author first name" required>
            <br>

            <label for="lastName">Surname:</label>
            <input type="text" name="lastName" id="lastName" placeholder="Enter author surname" required>
            <br>

            <label for="">Grade:</label>
            <input type="radio" name="grade" value="1" required>
            <label for="">1</label>
            <input type="radio" name="grade" value="2">
            <label for="">2</label>
            <input type="radio" name="grade" value="3">
            <label for="">3</label>
            <input type="radio" name="grade" value="4">
            <label for="">4</label>
            <input type="radio" name="grade" value="5">
            <label for="">5</label>
            <br>

            <input type="submit" name="submitButton">
        </form>
    </div>
    <footer>
        <h3>ICD007</h3>
    </footer>
</body>
</html>
